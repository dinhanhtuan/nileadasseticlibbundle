angular.module('nilead.module.shipping_method')
    .provider('ShippingMethod', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.shipping_method.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (shippingMethod, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.shipping_method.create'), shippingMethod, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.shipping_method.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (shippingMethod, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.shipping_method.update', {id: shippingMethod.id}), shippingMethod, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.shipping_method.delete', {id: id}))
                },

                getCalculators: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.shipping_method.get_calculators')).then(function (response) {
                        return response.data;
                    })
                }
            }
        }]
    });