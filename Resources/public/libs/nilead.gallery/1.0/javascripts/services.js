angular.module('nilead.module.gallery')
    .provider('Gallery', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.gallery.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.gallery.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (gallery, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.gallery.create'), gallery, success, error).then(function (response) {
                        return response.data;
                    });
                },

                update: function (gallery, data) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.gallery.update', {id: gallery.id}), data).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.gallery.delete', {id: id}))
                }
            }
        }]
    });