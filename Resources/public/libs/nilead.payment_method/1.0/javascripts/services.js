angular.module('nilead.module.payment_method')
    .provider('PaymentMethod', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.payment_method.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (paymentMethod, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.payment_method.create'), paymentMethod, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.payment_method.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (paymentMethod, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.payment_method.update', {id: paymentMethod.id}), paymentMethod, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.payment_method.delete', {id: id}))
                },

                getGateways: function () {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.payment_gateway_list')).then(function (response) {
                        return response.data;
                    })
                }
            }
        }]
    });