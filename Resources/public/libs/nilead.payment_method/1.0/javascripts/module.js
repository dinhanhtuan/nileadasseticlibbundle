angular.module('nilead.module.payment_method', ['nilead.module.resource', 'nilead.module.response_processor'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });