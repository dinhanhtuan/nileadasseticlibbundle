angular.module('nilead.module.party')
    .provider('Party', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.party.list', params)).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });