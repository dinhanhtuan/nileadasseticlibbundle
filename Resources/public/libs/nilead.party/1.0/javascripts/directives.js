angular.module('nilead.module.party')
    .directive('party', [function () {
        return {
            templateUrl: 'party.html',
            scope: {
                party: '='
            }
        };
    }]);