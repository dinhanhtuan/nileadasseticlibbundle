angular.module('nilead.module.tax_category')
    .provider('TaxCategory', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.tax_category.list', params)).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function(taxCategory, success, error){
                    return Resource.post(Routing.generate('en__RG__nilead.backend.tax_category.create'), taxCategory, success, error).then(function(response) {
                        return response.data;
                    });
                },
                
                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.tax_category.show', {id: id})).then(function(response) {
                        return response.data;
                    });
                },

                update: function(taxCategory, success, error){
                    return Resource.put(Routing.generate('en__RG__nilead.backend.tax_category.update',{id: taxCategory.id}), taxCategory, success, error).then(function(response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.tax_category.delete',{id: id}))
                }
            }
        }]
    });