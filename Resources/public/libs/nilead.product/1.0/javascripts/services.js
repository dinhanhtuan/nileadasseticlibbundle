angular.module('nilead.module.product')
    .provider('Product', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.product.list', params)).then(function (response) {
                        return response.data;
                    });
                },
                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.product.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });