angular.module('nilead.module.language')
    .provider('Language', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {

                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.language.list', params)).then(function (response) {
                        return response.data;
                    });
                },
                createNew: function (language, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.language.create'), language, success, error).then(function (response) {
                        return response.data;
                    });
                },

                show: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.language.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                update: function (language, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.language.update', {id: language.id}), language, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.language.delete', {id: id}))
                }
            }
        }]
    });