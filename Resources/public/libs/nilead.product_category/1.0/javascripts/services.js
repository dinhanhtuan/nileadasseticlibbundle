angular.module('nilead.module.product_category')
    .provider('ProductCategory', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.taxon.list_product_taxon', params)).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    })
;