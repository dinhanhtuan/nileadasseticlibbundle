angular.module('nilead.module.discount', ['nilead.module.resource', 'nilead.module.response_processor'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });