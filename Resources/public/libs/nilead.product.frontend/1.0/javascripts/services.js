angular.module('nilead.module.product.frontend')
    .provider('Product', function () {
        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                show: function (id) {
                    return Resource
                        .get(Routing.generate('en__RG__nilead.frontend.product_rest.show', {id: id}))
                        .then(function (response) {
                            return undefined !== response.data.product ? response.data.product : {};
                        })
                    ;
                }
            };
        }];
    })
;