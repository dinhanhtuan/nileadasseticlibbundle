angular.module('nilead.module.setting', ['nilead.module.resource', 'nilead.module.response_processor'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });