angular.module('nilead.module.customer')
    .provider('Customer', function () {

        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.customer.list', angular.extend(params, {_format: 'json'}))).then(function (response) {
                        return response.data;
                    });
                },
                get: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.customer.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                lock: function (id) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.customer.lock', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                unlock: function (id) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.customer.unlock', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                requestResetPassword: function (id) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.customer.request_reset_password', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                sendInvitation: function (id) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.customer.send_invitation', {id: id})).then(function (response) {
                        return response.data;
                    });
                },
                getOrders: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.customer.get_orders', {id: id})).then(function (response) {
                        return undefined !== response.data.orders ? response.data.orders : {}
                    });
                },

                createNew: function (customer, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.customer.create'), customer, success, error).then(function (response) {
                        return response.data;
                    });
                },

                update: function (customer, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.customer.update', {id: customer.id}), customer, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.customer.delete', {id: id})).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });