angular.module('nilead.module.file', [])
    .config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}');
    }])
;