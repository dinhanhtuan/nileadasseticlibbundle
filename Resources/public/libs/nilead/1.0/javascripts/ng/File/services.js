angular.module('nilead.module.file')
    .provider('File', function () {
        this.$get = [function () {
            return {
                change: function (event) {
                    var element = event.target;
                    var $element = angular.element(element);
                    var files = [];

                    if (element.multiple) {
                        angular.forEach(element.files, function(file, index){
                            this.push(file);
                        }, files);
                    } else {
                        files = element.files[0];
                    }

                    return files;
                },
                generateForm: function (model) {
                    var formData = new FormData();

                    var helper = function () {
                        return {
                            generateName: function (prefix, name) {
                                var segments = [];
                                angular.copy(prefix, segments);
                                segments.push(name);
                                name = segments.shift();

                                while (segments.length) {
                                    name += '[' + segments.shift() + ']';
                                }

                                return name;
                            },
                            addFields: function (fields, namePrefix) {
                                angular.forEach(fields, function (field, name) {
                                    if ('string' === typeof name && '$' === name.substr(0, 1)) {
                                        return;
                                    }

                                    if ('array' !== typeof field && 'object' !== typeof field) {
                                        this.append(helper.generateName(namePrefix, name), field);
                                    } else if (field instanceof File) {
                                        this.append(helper.generateName(namePrefix, name), field);
                                    } else {
                                        var prefix = [];
                                        angular.copy(namePrefix, prefix);
                                        prefix.push(name);

                                        helper.addFields(field, prefix);
                                    }
                                }, formData);
                            }
                        }
                    }();

                    helper.addFields(model, []);

                    return formData;
                }
            }
        }]
    })
    .directive('nlFile', ['File', '$parse', '$rootScope', function(File, $parse, $rootScope) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function (event) {
                    var file = File.change(event);
                    var model = $parse(attributes.nlModel);

                    model.assign(scope, file);
                    scope.$apply();

                    $rootScope.$broadcast('NILEAD_FILE_UPLOAD_BROWSED', element, scope.$index, file);
                }).trigger('click');
            }
        };
    }])
    .directive('nlBrowse', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('click', function (event) {
                    element.parent().find('[type="file"]').trigger('click');
                });
            }
        };
    })
    .directive('nlThumb', function () {
        return {
            restrict: 'A',
            template: '<img />',
            link: function (scope, element, attributes) {
                scope.$watch(attributes.nlThumb, function (file) {
                    if (file instanceof File) {
                        var img = element.find('img');
                        var reader = new FileReader();

                        // Closure to capture the file information.
                        reader.onload = function(event) {
                            img.attr('src', event.target.result);
                        };

                        // Read in the image file as a data URL.
                        reader.readAsDataURL(file);
                    }
                });
            }
        }
    })
;