angular.module('nilead.module.alert')
    .provider('Alert', function () {
        var alerts = {}

        this.$get = function () {

            var _alerts = alerts;

            return {
                add: function (type, field, msg, level) {
                    if (angular.isUndefined(_alerts[type])) {
                        _alerts[type] = {};
                    }

                    if (angular.isUndefined(_alerts[type][field])) {
                        _alerts[type][field] = [];
                    }

                    if (angular.isUndefined(level)) {
                        level = 'error';
                    }

                    _alerts[type][field].push({message: msg, level: level});
                },
                get: function () {
                    return _alerts;
                },
                close: function (type, field) {
                    if (angular.isDefined(field)) {
                        delete _alerts.type.field;
                    } else if (angular.isDefined(type)) {
                        delete _alerts.type;
                    }
                },
                reset: function () {
                    angular.copy({}, _alerts);
                }
            }
        }
    });