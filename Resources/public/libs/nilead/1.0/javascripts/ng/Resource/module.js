angular.module('nilead.module.resource', ['restangular', 'nilead.module.response_processor'])
    .config(['$interpolateProvider', 'RestangularProvider', function ($interpolateProvider, RestangularProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}');

        return RestangularProvider.setDefaultHeaders({
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        });
    }]);