angular.module('nilead.module.searcher', ['nilead.module.resource', 'nilead.module.response_processor'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });