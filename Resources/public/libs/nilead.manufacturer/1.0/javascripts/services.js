angular.module('nilead.module.manufacturer')
    .provider('Manufacturer', function () {

        this.$get = ['responseProcessor', 'Resource', function (responseProcessor, Resource) {
            return {
                list: function (params) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.manufacturer.list', params)).then(function (response) {
                        return response.data;
                    });
                },
                get: function (id) {
                    return Resource.get(Routing.generate('en__RG__nilead.backend.manufacturer.show', {id: id})).then(function (response) {
                        return response.data;
                    });
                },

                createNew: function (manufacturer, success, error) {
                    return Resource.post(Routing.generate('en__RG__nilead.backend.manufacturer.create'), manufacturer, success, error).then(function (response) {
                        return response.data;
                    });
                },

                update: function (manufacturer, success, error) {
                    return Resource.put(Routing.generate('en__RG__nilead.backend.manufacturer.update', {id: manufacturer.id}), manufacturer, success, error).then(function (response) {
                        return response.data;
                    });
                },

                remove: function (id) {
                    return Resource.remove(Routing.generate('en__RG__nilead.backend.manufacturer.delete', {id: id})).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    });