angular.module('nilead.module.manufacturer', ['nilead.module.resource', 'nilead.module.response_processor'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });