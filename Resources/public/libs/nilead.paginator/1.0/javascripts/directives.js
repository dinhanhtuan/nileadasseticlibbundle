angular.module('nilead.module.paginator')
    .directive('paginator', ['$state', '$stateParams', function ($state, $stateParams) {
        return {
            templateUrl: 'paginator.html',
            scope: {
                pg: '='
            },
            link: function (scope, elem, attrs) {

                scope.$watch('pg', function(pg) {
                    if(pg && pg.total_count) {
                        scope.pg.page_count  = Math.ceil(pg.total_count / pg.num_items_per_page);

                        if (pg.page_range > scope.pg.page_count) {
                            pg.page_range = scope.pg.page_count;
                        }

                        var $proximity = Math.floor(pg.page_range / 2);
                        var $delta = Math.ceil(pg.page_range / 2);

                        if (pg.current_page_number - $delta > scope.pg.page_count - pg.page_range) {
                            var $pages = _.range(scope.pg.page_count - pg.page_range + 1, scope.pg.page_count + 1);
                        } else {
                            if (pg.current_page_number - $delta < 0) {
                                $delta = pg.current_page_number;
                            }

                            var $offset = pg.current_page_number - $delta;
                            var $pages = _.range($offset + 1, $offset + pg.page_range + 1);
                        }

                        scope.pg.pages_in_range  = $pages;
                        scope.pg.start_page  = +pg.current_page_number - +$proximity;
                        scope.pg.end_page    = +pg.current_page_number + +$proximity;
                        if (scope.pg.start_page < 1) {
                            scope.pg.end_page = Math.min(scope.pg.end_page + (1 - scope.pg.start_page), scope.pg.page_count);
                            scope.pg.start_page = 1;
                        }

                        if (scope.pg.end_page > scope.pg.page_count) {
                            scope.pg.start_page = Math.max(scope.pg.start_page - (scope.pg.end_page - scope.pg.page_count), 1);
                            scope.pg.end_page = scope.pg.page_count;
                        }

                        if (scope.pg.current_page_number - 1 > 0) {
                            scope.pg.previous      = +pg.current_page_number - 1;
                        }

                        if (scope.pg.current_page_number + 1 <= scope.pg.page_count) {
                            scope.pg.next          = +pg.current_page_number + 1;
                        }
                    }

                    scope.link = function (stateParams) {
                        return $state.href(attrs.route, angular.extend({}, $stateParams, stateParams));
                    }
                });
            }
        };
    }]);
